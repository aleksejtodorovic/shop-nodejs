const deleteProduct = async btn => {
    try {
        const productId = btn.parentNode.querySelector('[name=productId]').value;
        const csrfToken = btn.parentNode.querySelector('[name=_csrf]').value;


        const response = await fetch(`/admin/product/${productId}`, {
            method: 'DELETE',
            headers: {
                'csrf-token': csrfToken
            }
        });
        const { message } = await response.json();

        if (message === 'Success!') {
            const productElement = btn.closest('article');

            productElement.parentNode.removeChild(productElement);
        }
    } catch (ex) {
        console.log(ex);
    }
};