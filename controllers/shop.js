const fs = require('fs');
const path = require('path');
const PDFDocument = require('pdfkit');
const stripe = require('stripe')('sk_test_U0KlIxKBcTLYSnSZ6nNMbrPi00hNDDNe6A');

const Product = require('../models/product');
const Order = require('../models/order');

const ITEMS_PER_PAGE = 10;

exports.getIndex = async (req, res, next) => {
    try {
        const page = parseInt(req.query.page, 10) || 1;
        const totalProducts = await Product.find().countDocuments();
        const hasNextPage = ITEMS_PER_PAGE * page < totalProducts;
        const hasPerviousPage = page > 1;
        const lastPage = Math.ceil(totalProducts / ITEMS_PER_PAGE);
        const products = await Product.find().skip((page - 1) * ITEMS_PER_PAGE).limit(ITEMS_PER_PAGE);

        return res.render('shop/index', { products, pageTitle: 'Shop', path: '/', hasPerviousPage, hasNextPage, page, lastPage });
    } catch (ex) {
        return next(new Error(ex));
    }
};

exports.getProducts = async (req, res, next) => {
    try {
        const page = parseInt(req.query.page, 10) || 1;
        const totalProducts = await Product.find().countDocuments();
        const hasNextPage = ITEMS_PER_PAGE * page < totalProducts;
        const hasPerviousPage = page > 1;
        const lastPage = Math.ceil(totalProducts / ITEMS_PER_PAGE);
        const products = await Product.find().skip((page - 1) * ITEMS_PER_PAGE).limit(ITEMS_PER_PAGE);

        return res.render('shop/product-list', { products, pageTitle: 'Shop', path: '/products', hasPerviousPage, hasNextPage, page, lastPage });
    } catch (ex) {
        return next(new Error(ex));
    }
};

exports.getProduct = async (req, res, next) => {
    try {
        const { productId } = req.params;
        const product = await Product.findById(productId);

        return res.render('shop/product-details', { product, pageTitle: 'Product Details', path: '/products' });
    } catch (ex) {
        return next(new Error(ex));
    }
};

exports.getCart = async (req, res, next) => {
    try {
        const { cart: { items: products } } = await req.user.populate('cart.items.product').execPopulate();

        return res.render('shop/cart', { products, pageTitle: 'Your Cart', path: '/cart' });
    } catch (ex) {
        return next(new Error(ex));
    }
};

exports.postCart = async (req, res, next) => {
    try {
        const { productId } = req.body;
        const productIndex = req.user.cart.items.findIndex(cartItem => cartItem.product.toString() === productId.toString());

        if (productIndex >= 0) {
            const product = req.user.cart.items[productIndex];

            product.quantity = product.quantity + 1;
        } else {
            req.user.cart.items.push({ product: productId, quantity: 1 });
        }

        await req.user.save();

        return res.redirect('/cart');
    } catch (ex) {
        return next(new Error(ex));
    }
};

exports.postCartDeleteItem = async (req, res, next) => {
    try {
        const { productId } = req.body;
        const productIndex = req.user.cart.items.findIndex(item => item.product.toString() === productId.toString());

        if (productIndex >= 0) {
            req.user.cart.items.splice(productIndex, 1);
            await req.user.save();
        }

        return res.redirect('/cart');
    } catch (ex) {
        return next(new Error(ex));
    }
};

exports.getOrders = async (req, res, next) => {
    const orders = await Order.find({ 'user.userId': req.user._id });

    return res.render('shop/orders', { orders, pageTitle: 'Your Orders', path: '/orders' });
};

exports.getOrderInvoice = async (req, res, next) => {
    try {
        const { orderId } = req.params;
        const order = await Order.findById(orderId);

        if (!order) {
            return next(new Error('Order not found!'));
        }

        if (order.user.userId.toString() !== req.user._id.toString()) {
            return next(new Error('Unauthorized!'));
        }

        const invoiceName = `invoice-${orderId}.pdf`;
        const invoicePath = path.join('data', 'invoices', invoiceName);

        const pdfDoc = new PDFDocument();

        res.setHeader('Content-Type', 'plain/text');
        res.setHeader('Content-Disposition', `inline; filename=${invoiceName}`);

        pdfDoc.pipe(fs.createWriteStream(invoicePath));
        pdfDoc.pipe(res);

        pdfDoc.fontSize(26).text('Invoice', {
            underline: true
        });

        pdfDoc.text('-----------------------');

        let totalPrice = 0;

        order.products.forEach(({ product: { title, price }, quantity }) => {
            totalPrice += quantity * price;
            pdfDoc.fontSize(14).text(`${title} - ${quantity} x $${price}`);
        });

        pdfDoc.text('-----------');
        pdfDoc.fontSize(20).text(`Total Price: $${totalPrice}`);

        pdfDoc.end();

        // fs.readFile(invoicePath, (err, data) => {
        //     if (err) {
        //         return next(err);
        //     }

        //     res.setHeader('Content-Type', 'plain/text');
        //     res.setHeader('Content-Disposition', `inline; filename=${invoiceName}`);
        //     res.send(data);
        // });
    } catch (ex) {
        return next(new Error(ex));
    }
};

exports.getCheckoutSuccess = async (req, res, next) => {
    try {
        const user = await req.user.populate('cart.items.product').execPopulate();

        await new Order({
            products: user.cart.items.map(({ quantity, product: { title, price } }) => ({ product: { title, price }, quantity })),
            user: {
                name: req.user.name,
                userId: req.user._id
            }
        }).save();

        req.user.cart.items = [];
        await req.user.save();

        return res.redirect('/orders');
    } catch (ex) {
        return next(new Error(ex));
    }
};

exports.getCheckout = async (req, res, next) => {
    try {
        const { cart: { items: products } } = await req.user.populate('cart.items.product').execPopulate();
        const totalSum = products.reduce((totalSum, { product, quantity }) => { return totalSum + (product.price * quantity) }, 0);
        const stripeSession = await stripe.checkout.sessions.create({
            payment_method_types: ['card'],
            line_items: products.map(({ quantity, product }) => {
                return {
                    name: product.title,
                    description: product.description,
                    amount: product.price * 100, //must be in cents
                    currency: 'usd',
                    quantity
                };
            }),
            success_url: `${req.protocol}://${req.get('host')}/checkout/success`,
            cancel_url: `${req.protocol}://${req.get('host')}/checkout/cancel`
        });

        return res.render('shop/checkout', { pageTitle: 'Checkout', path: '/checkout', products, totalSum, sessionId: stripeSession.id });
    } catch (ex) {
        return next(new Error(ex));
    }
};