const bcrypt = require('bcryptjs');
const sgMail = require('@sendgrid/mail');
const crypto = require('crypto');
const {
    validationResult
} = require('express-validator');

const User = require('../models/user');

sgMail.setApiKey(process.env.SENDGRID_KEY);

exports.getLogin = (req, res, next) => {
    if (req.session.isLoggedIn) {
        return res.redirect('/');
    }

    return res.render('auth/login', {
        pageTitle: 'Login',
        path: '/login',
        errorMessages: req.flash('error'),
        oldInput: {
            email: '',
            password: ''
        }
    });
};

exports.postLogin = async (req, res, next) => {
    try {
        const {
            email,
            password
        } = req.body;
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(422).render('auth/login', {
                path: '/login',
                pageTitle: 'Login',
                errorMessages: errors.array({
                    onlyFirstError: true
                }),
                oldInput: {
                    email,
                    password
                }
            });
        }

        const user = await User.findOne({
            email
        });

        if (!user) {
            req.flash('error', 'Invalid email or password.');
            return res.redirect('/login');
        }

        const passwordMatched = await bcrypt.compare(password, user.password);

        if (!passwordMatched) {
            req.flash('error', 'Invalid email or password.');
            return res.redirect('/login');
        }

        req.session.isLoggedIn = true;
        req.session.user = {
            _id: user._id
        };
        await req.session.save();

        return res.redirect('/');
    } catch (ex) {
        return next(new Error(ex));
    }
};

exports.getSignup = (req, res, next) => {
    return res.render('auth/signup', {
        pageTitle: 'Signup',
        path: '/signup',
        errorMessages: req.flash('error'),
        oldInput: {
            name: '',
            email: '',
            password: '',
            confirmPassword: ''
        }
    });
};

exports.postSignup = async (req, res, next) => {
    try {
        const {
            name,
            email,
            password,
            confirmPassword
        } = req.body;
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(422).render('auth/signup', {
                pageTitle: 'Signup',
                path: '/signup',
                errorMessages: errors.array({
                    onlyFirstError: true
                }),
                oldInput: {
                    name,
                    email,
                    password,
                    confirmPassword
                }
            });
        }

        const hashedPassword = await bcrypt.hash(password, 12);

        const user = await new User({
            name,
            email,
            password: hashedPassword,
            cart: {
                items: []
            }
        }).save();

        req.session.isLoggedIn = true;
        req.session.user = {
            _id: user._id
        };
        await req.session.save();
        // sgMail.send({
        //     to: email,
        //     from: 'shop@node-course.com',
        //     subject: 'Signup completed!',
        //     html: '<h1>You successfully signed up! New module.</h1>'
        // }).catch(ex => {
        //     const error = new Error(ex);

        //     error.httpStatusCode = 500;
        //     return next(error);
        // });

        return res.redirect('/');
    } catch (ex) {
        return next(new Error(ex));
    }
};

exports.postLogout = (req, res, next) => {
    try {
        req.session.destroy(() => {
            return res.redirect('/login');
        });
    } catch (ex) {
        return next(new Error(ex));
    }
};

exports.getNewPassword = async (req, res, next) => {
    try {
        const token = req.params.token;
        const user = await User.findOne({
            resetToken: token,
            resetTokenExpiration: {
                $gt: Date.now()
            }
        });

        if (!user) {
            return res.redirect('/login');
        }

        return res.render('auth/new-password', {
            pageTitle: 'New Password',
            path: '/new-password',
            errorMessages: req.flash('error'),
            userId: user._id,
            passwordToken: token
        });
    } catch (ex) {
        return next(new Error(ex));
    }
};

exports.postNewPassword = async (req, res, next) => {
    try {
        const {
            password,
            userId,
            passwordToken
        } = req.body;
        const user = await User.findOne({
            _id: userId,
            resetToken: passwordToken,
            resetTokenExpiration: {
                $gt: Date.now()
            }
        });

        if (!user) {
            return res.redirect('/login');
        }

        const hashedPassword = await bcrypt.hash(password, 12);

        user.password = hashedPassword;
        user.resetToken = undefined;
        user.resetTokenExpiration = undefined;

        await user.save();

        sgMail.send({
            to: user.email,
            from: 'shop@node-course.com',
            subject: 'New Password',
            html: `
                <p>You have setted new password!</p
            `
        }).catch(ex => {
            const error = new Error(ex);

            error.httpStatusCode = 500;
            return next(error);
        });

        return res.redirect('/login');
    } catch (ex) {
        return next(new Error(ex));
    }
};

exports.getResetPassword = (req, res, next) => {
    return res.render('auth/reset-password', {
        pageTitle: 'Reset Password',
        path: '/reset-password',
        errorMessages: req.flash('error')
    });
}

exports.postResetPassword = (req, res, next) => {
    try {
        crypto.randomBytes(32, async (err, buffer) => {
            if (err) {
                console.log(err);
                return res.redirect('/reset-password');
            }

            const {
                email
            } = req.body;
            const token = buffer.toString('hex');
            const user = await User.findOne({
                email
            });

            if (!user) {
                req.flash('error', 'No account with that email found.');
                return res.redirect('/reset-password');
            }

            user.resetToken = token;
            user.resetTokenExpiration = Date.now() + 3600000; // 1 hour
            await user.save();

            sgMail.send({
                to: email,
                from: 'shop@node-course.com',
                subject: 'Password Reset',
                html: `
                    <p>You requested a password reset</p
                    <p>Click this <a href="http://localhost:3000/reset-password/${token}">link</a> to set a new password</p>
                `
            }).catch(ex => {
                const error = new Error(ex);

                error.httpStatusCode = 500;
                return next(error);
            });

            return res.redirect('/');
        });
    } catch (ex) {
        return next(new Error(ex));
    }
};