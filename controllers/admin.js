const { validationResult } = require('express-validator');

const Product = require('../models/product');
const { deleteFile } = require('../util/file');

exports.getAddProduct = (req, res, next) => {
    return res.render('admin/add-product', {
        pageTitle: 'Add Product',
        path: '/admin/add-product',
        errorMessages: req.flash('error'),
        oldInput: {
            title: '',
            description: '',
            price: ''
        }
    });
};

exports.postAddProduct = async (req, res, next) => {
    try {
        const { title, description, price } = req.body;
        const { file: image } = req;
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(422).render('admin/add-product', {
                pageTitle: 'Add Product',
                path: '/admin/add-product',
                errorMessages: errors.array({ onlyFirstError: true }),
                oldInput: {
                    title,
                    description,
                    price
                }
            });
        }

        if (!image) {
            return res.status(422).render('admin/add-product', {
                pageTitle: 'Add Product',
                path: '/admin/add-product',
                errorMessages: ['Attached file is not an image.'],
                oldInput: {
                    title,
                    description,
                    price
                }
            });
        }

        const imageUrl = image.path;
        const product = new Product({ title, price, description, imageUrl, userId: req.user });

        await product.save();

        return res.redirect('/admin/products');
    } catch (ex) {
        return next(new Error(ex));
    }
};

exports.getProducts = async (req, res, next) => {
    try {
        const products = await Product.find({ userId: req.user._id }).populate('userId', 'name email -_id');

        return res.render('admin/products', { products, pageTitle: 'Admin Products', path: '/admin/products' });
    } catch (ex) {
        return next(new Error(ex));
    }
};

exports.getEditProduct = async (req, res, next) => {
    try {
        const { productId } = req.params;
        const product = await Product.findById(productId);

        return res.render('admin/edit-product', { product, errorMessages: [], pageTitle: 'Edit Product', path: '/admin/edit-product' });
    } catch (ex) {
        return next(new Error(ex));
    }
};

exports.postEditProduct = async (req, res, next) => {
    try {
        const { productId: _id } = req.params;
        const { title, price, description } = req.body;
        const { file: image } = req;
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(422).render('admin/edit-product', {
                pageTitle: 'Edit Product',
                path: '/admin/add-product',
                errorMessages: errors.array({ onlyFirstError: true }),
                product: {
                    _id,
                    title,
                    description,
                    price
                }
            });
        }

        const product = await Product.findOne({ _id });

        if (product.userId.toString() !== req.user._id.toString()) {
            return res.redirect('/');
        }

        product.title = title;

        if (image) {
            deleteFile(product.imageUrl);
            product.imageUrl = image.path;
        }

        product.price = price;
        product.description = description;
        await product.save();

        return res.redirect('/admin/products');
    } catch (ex) {
        return next(new Error(ex));
    }
};

exports.deleteProduct = async (req, res, next) => {
    try {
        const { productId } = req.params;
        const product = await Product.findOne({ _id: productId, userId: req.user._id });

        if (!product) {
            return res.status(404).json({
                message: 'Product with given id not found.'
            });
        }

        deleteFile(product.imageUrl);

        await Product.deleteOne({ _id: productId, userId: req.user._id });

        return res.status(200).json({
            message: "Success!"
        });
    } catch (ex) {
        return res.status(500).json({
            message: 'Deleting product failed.'
        });
    }
};