const express = require('express');
const { body } = require('express-validator');

const { getAddProduct, postAddProduct, getProducts, getEditProduct, postEditProduct, deleteProduct } = require('../controllers/admin');
const isAuth = require('../middleware/is-auth');

const router = express.Router();

// /admin/add-product GET
router.get('/add-product', isAuth, getAddProduct);

// // /admin/add-product POST
router.post(
    '/add-product',
    isAuth,
    [
        body('title', 'Title is required with minimum length of 3 characters.')
            .isString()
            .isLength({ min: 3 })
            .trim(),
        body('description', 'Description must be at least 5 characters, not more than 400.')
            .isLength({ min: 5, max: 400 }),
        body('price', 'Price must be greater than 0.')
            .isFloat()
            .custom(value => {
                return value > 0;
            })
    ],
    postAddProduct
);

router.get('/products', isAuth, getProducts);

router.get('/edit-product/:productId', isAuth, getEditProduct);

router.post('/edit-product/:productId',
    isAuth,
    [
        body('title')
            .isString()
            .isLength({ min: 3 })
            .trim(),
        body('description', 'Description must be at least 5 characters, not more than 400')
            .isLength({ min: 5, max: 400 }),
        body('price', 'Price is required and must be greater than 0.')
            .isFloat()
            .custom(value => {
                return value > 0;
            })
    ],
    postEditProduct);

router.delete('/product/:productId', isAuth, deleteProduct);

module.exports = router;