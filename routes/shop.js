const express = require('express');

const { getIndex, getProducts, getCart, postCart, postCartDeleteItem, getCheckout, getOrders, getCheckoutSuccess, getProduct, getOrderInvoice } = require('../controllers/shop');
const isAuth = require('../middleware/is-auth');

const router = express.Router();

router.get('/', getIndex);

router.get('/products', getProducts);

router.get('/products/:productId', getProduct);

router.get('/cart', isAuth, getCart);

router.post('/cart-delete-item', isAuth, postCartDeleteItem);

router.post('/cart', isAuth, postCart);

router.get('/checkout', isAuth, getCheckout);

router.get('/checkout/success', isAuth, getCheckoutSuccess);

router.get('/checkout/cancel', isAuth, getCheckout);

router.get('/orders', isAuth, getOrders);

router.get('/orders/:orderId', isAuth, getOrderInvoice);

module.exports = router;