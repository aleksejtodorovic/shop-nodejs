const { Router } = require('express');
const { check, body } = require('express-validator');

const { getLogin, postLogin, postLogout, getSignup, postSignup, getNewPassword, postNewPassword, getResetPassword, postResetPassword } = require('../controllers/auth');
const User = require('../models/user');

const router = new Router();

router.get('/login', getLogin);

router.post(
    '/login',
    [
        body('email')
            .isEmail()
            .withMessage('Please enter a valid email address.')
            .normalizeEmail(),
        body('password', 'Password has to be valid.')
            .isLength({ min: 5 })
            .isAlphanumeric()
            .trim()
    ],
    postLogin
);

router.get('/signup', getSignup);

router.post(
    '/signup',
    [
        body('name')
            .isAlphanumeric()
            .withMessage('Name should be alphanumberic'),
        check('email')
            .isEmail()
            .withMessage('Please enter a valid email.')
            .custom(async (value, { req }) => {
                let user = await User.findOne({ email: value });

                if (user) {
                    throw new Error('Email is taken, please use a different one.');
                }

                return true;
            })
            .normalizeEmail(),
        body(
            'password',
            'Please enter a password with only numbers and text and it must be at least 5 characters.'
        )
            .isLength({
                min: 5
            })
            .isAlphanumeric()
            .trim(),
        body('confirmPassword')
            .custom((value, { req }) => {
                if (value !== req.body.password) {
                    throw new Error('Passwords have to match.');
                }

                return true;
            })

    ],
    postSignup
);

router.post('/logout', postLogout);

router.get('/reset-password/:token', getNewPassword);

router.post('/new-password', postNewPassword);

router.get('/reset-password', getResetPassword);

router.post('/reset-password', postResetPassword);

module.exports = router;